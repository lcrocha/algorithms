#include <vector>
typedef std::vector<int> VECTOR;

void QuickSort(VECTOR::iterator& lo, VECTOR::iterator& hi)
{
	auto i = lo;
	auto j = hi;
	auto pivot = hi;
	while (i < j)
	{
		if (*i <= *pivot)
		{
			++i;
		}
		else if (*j >= *pivot)
		{
			--j;
		}
		else
		{
			std::swap(*i, *j);
		}
	}
	//Changing pivot's position
	if (*i != *pivot)
	{
		std::swap(*i, *pivot);
	}
	//Spliting
	if (i > lo)
	{
		QuickSort(lo, i - 1);
	}
	if (j < hi)
	{
		QuickSort(j + 1, hi);
	}
}