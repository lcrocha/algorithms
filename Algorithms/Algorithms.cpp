// Algorithms.cpp : Defines the entry point for the console application.
//
#include <algorithm>
#include <vector>
typedef std::vector<int> VECTOR;

int lrand()
{
	return rand() % 100;
}

VECTOR v(55), r;

void QuickSort(VECTOR::iterator& lo, VECTOR::iterator& hi);

void main(void)
{
	std::generate(v.begin(), v.end(), lrand);
	/*v[0] = 15;
	v[v.size() / 2] = 15;*/
	//v.push_back(1);
	r = v;

	QuickSort(v.begin(), v.end() - 1);
}

