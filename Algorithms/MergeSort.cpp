#include <vector>
typedef std::vector<int> VECTOR;

void MergeSort(VECTOR& res, VECTOR& v0)
{
	if(v0.size() > 1)
	{
		int half = v0.size()/2;
		//Spliting
		VECTOR res1, v1(v0.begin(), v0.begin() + half);
		MergeSort(res1, v1);
		VECTOR res2, v2(v0.begin() + half, v0.end());
		MergeSort(res2, v2);
		//Merging
		auto it1 = res1.begin();
		auto it2 = res2.begin();
		while(it1 != res1.end() && it2 != res2.end())
		{
			if(*it1 <= *it2)
			{
				res.push_back(*it1);
				++it1;
			}
			else if(*it2 < *it1)
			{
				res.push_back(*it2);
				++it2;
			}
		}
		//Pushing remaining
		if(it1 != res1.end())
		{
			res.insert(res.end(), it1, res1.end());
		}
		else if(it2 != res2.end())
		{
			res.insert(res.end(), it2, res2.end());
		}
	}
	else
	{
		res = v0;
	}
}